---
title: 'Rules and Operating Procedures for the Complaint Review Board'
documentclass: book
geometry: margin=1in
pagestyle: headings
papersize: letter
fontsize: 12pt
toc-depth: 1
...

From [the Omnibus](./Omnibus.html){.compatibility}:

> The Complaint Review Board (henceforth referred to as the CRB) is a group of
  five primary members and three alternate members tasked with receiving
  complaints of potential violations of Realms rules (including the [Code of
  Conduct](./Omnibus.html#code-of-conduct){.compatibility} and the [Rules for
  Event Holders](./Omnibus.html#rules-for-event-holders){.compatibility}) and
  investigating their validity. Following the investigation, they will present
  the matter to the Event Holders of the Realms.

The contents of this page describe the policies and procedures for the
Complaint Review Board.

## Submitting and Receiving Complaints ## {- #submitting-and-receiving-complaints}

Any member of the Realms community may submit a complaint to the CRB by
emailing <complaints@lists.realmsnet.net>. The party submitting the complaint
(henceforth referred to as the Complainant) must, at a minimum, include the
following information:

* The name of the Complainant.
* The name(s) of the individual(s) who the Complainant believes to have
  violated one or more rules (henceforth referred to as the
  Defendant(s)).
* A description of the alleged violation.
* If the alleged violation(s) occurred at a Realms event, the name of that
  event.

If the CRB receives a complaint which contains some but not all of the required
information, they will attempt to contact the Complainant and collect the
remaining information.

The Complainant may also include additional information with their complaint,
such as:

* A timeline of events that describes the alleged violation(s).
* The names of other individuals who were involved in and/or affected by the
  alleged violation(s), including witnesses and/or individuals who have intimate
  knowledge related to the incident.
* Any other information which the Complainant feels is related to the
  complaint.

If the Complainant does not include any of this optional information when they
initially submit the complaint, they may still choose to provide such
information at any time during an investigation.

The CRB will address complaints submitted within one year from the incident in
question. The CRB may, at their discretion, address complaints submitted more
than one year from the incident in question.

## Protection of Privacy ## {- #protection-of-privacy}

Any time the CRB asks an individual if they would like to be anonymous, the
individual will be given at least 48 hours to respond. If the individual does
not explicitly request or decline anonymity within that time, the CRB will
proceed as if the individual requested anonymity. The CRB will not conduct any
business with an individual before they’ve received a response to this
question.

The CRB must, to the greatest reasonable extent, protect the privacy of all
parties involved in the complaint even if they do not request anonymity. Outside
of internal communications, the CRB will only refer to involved individuals by
name when necessary; otherwise, they will refer to involved individuals via
designations such as “the Complainant”, “the Defendant(s)”, “person 1”, “person
2”, etc. Additionally, outside of internal communications, any circumstances
related to the complaint will only be described with the minimum necessary
detail, so as to prevent compromising individuals' anonymity.

In the course of their normal business, the CRB may conduct interviews with
individuals. The CRB cannot decline to interview a party solely because they
have requested anonymity. Additionally, the CRB cannot decline to interview a
party solely because conducting the interview would create a conflict of
interest for a member of the CRB. If the CRB must interview a party to the
complaint and doing so would create a conflict of interest for a member of the
CRB, that member must recuse themself.

## General Decision Making Process ## {- #general-decision-making-process}

The CRB makes decisions by majority vote. While certain policies in this
section require a vote to be called, the Chairperson also has the authority to
call for a vote on any matter. Votes are decided by simple majority, excluding
abstentions. The Chairperson may cast a vote even if they called for the vote,
but their vote is not considered to be more important than that of any other
member.

If at any point a member of the CRB finds that they have a conflict of interest
with a vote, they must recuse themself from that vote. If the conflict of
interest will persist through the course of a CRB process, the member must
recuse themself from that process. Whenever a member is recused, they must be
replaced by an alternate for the duration of the recusal.

The CRB must always have at least five members available to conduct its
business. If at any time fewer than five members are available, the CRB must
halt the current process and call for an [Emergency EH
Meeting](./Omnibus.html#emergency-eh-meetings){.compatibility}. A meeting called
for this reason bypasses the requirement that a certain number of EHs need to
agree to hold the meeting and must either elect enough temporary members to the
CRB to reach the required five members or require that the complaint shall be
addressed by all present EHs not involved in the complaint, rather than the
CRB.

## Communication Policies ## {- #communication-policies}

The CRB communicates with several parties as it conducts its business. Unless
otherwise specified, the CRB will allow 48 hours for individuals to respond to
CRB communication. Similarly, the CRB will respond to members of the Realms
community within 48 hours. In either case, if the respondent(s) require more
than 48 hours to respond, they must acknowledge receipt of the communication to
its sender within 48 hours and provide an estimated timeline in which they
expect to provide a full response.

## Investigative Process ## {- #investigative-process}

Upon receiving a complaint, the CRB will contact the Complainant to acknowledge
receipt within 48 hours. At the same time, they will ask the Complainant if they
would like to remain anonymous. At any point during the Investigative Process,
the Complainant may withdraw their complaint, at which point the CRB shall cease
their investigation and inform anyone they had interviewed that the
investigation has ended.

After acknowledging receipt of the complaint, the CRB will begin their
investigative process by communicating with the Complainant, the Defendant(s),
and any other relevant or necessary parties to provide each of them with the
following information:

* The name of the Complainant, if the Complainant declines anonymity.
* The name of the Defendant(s).
* The nature of the alleged violation(s).
* An outline of the investigative process, including any anticipated
  modifications, exceptions, and/or relevant internal policies.
* An estimated timeline for the investigation.

The CRB will investigate all complaints regardless of perceived
triviality.

Relevant notifications must be made within seven days of the CRB acknowledging
receipt of the complaint. If the CRB is unable to meet this deadline, they will
immediately contact the complainant and provide an expected timeline along with
a justification for such.

After notifying the relevant parties, they will begin an investigation of the
alleged violation(s). The CRB will interview the Complainant, the Defendant(s),
and other parties and/or witnesses to the case. At any point during the
investigation, the Complainant, the Defendant(s), or the CRB itself may identify
other additional parties who are related to the case.

If, during the course of the investigation, the CRB modifies an existing policy
or enacts a previously unmentioned policy, they must contact the Complainant,
the Defendant(s), and any other relevant or necessary parties to inform them of
the changes.

The CRB will make reasonable efforts to contact and interview parties named by
the Complainant and Defendant(s), however, the CRB may decline to interview an
individual who is found to be unresponsive or uncooperative. The CRB will allow
individuals at least 48 hours to respond to requests for interviews, although
the actual interview may take place outside of that 48 hour period. Interviews
can be conducted over any mutually agreeable platform (the CRB should take
reasonable measures to accommodate the preferences of a party to be interviewed)
and must be recorded in some way.

The CRB may determine that the nature of the complaint and the alleged
violation is consistent with a pattern of behavior on the part of the
Complainant and/or Defendant(s). In making such a determination, the CRB may
consider (among other things): previous complaints received by the CRB, previous
actions and investigations conducted by Realms administrative bodies, personal
observation, and witness testimony. If the CRB is investigating a possible
pattern of behavior, they may interview individuals who were not originally
named in the complaint in order to gather additional evidence.

The CRB should, to the greatest reasonable extent, strive to conclude their
investigation within the 30 days which follow the CRB notifying parties that an
investigation is underway. If the CRB is unable to meet this deadline, they will
immediately (and every 7 days thereafter until the investigation is complete)
contact the Complainant, the Defendant(s), and the Event Holders' List to
provide the following information:

* The date on which the CRB initiated the investigation.
* The reason for which the CRB has not yet been able to conclude the
  investigation.
* The date on which the CRB expects to conclude the investigation.

As soon as the CRB concludes an investigation, they will notify [all parties
who are eligible to receive the investigation report](#eligible-parties) of this
status and present an estimated timeline for the creation and distribution of
the report.

## Reporting on Investigations ## {- #reporting-on-investigations}

Upon concluding their investigation, the CRB will generate a report describing
the investigation. The report will, at a minimum, contain the following
information:

* The date on which the complaint was received.
* The dates on which the investigation was started and ended.
* The name of any CRB member who recused themself, the date on which the
  recusal took place, and the name of the alternate member who replaced
  them.
* The name of the Complainant, if the Complainant declines anonymity.
* The name of the Defendant(s).
* The nature of the alleged violation(s).
* An outline of the investigative process, including any modifications,
  exceptions, and/or relevant internal policies.
* The number of anonymous individuals interviewed as part of the investigation,
  the names of any individuals who were interviewed and declined anonymity, and
  the date(s) of the interview(s) and the associated record(s).
* The name of every individual who was identified as a party to the case but
  who was not interviewed, along with justification for why the individual was
  not interviewed and the date(s) on which the individual was contacted by the
  CRB with a request for an interview.
* Any other evidence associated with the case.
* The result of every vote called by the CRB, along with the date on which the
  vote was called, the options being voted upon, and the way in which each
  member of the CRB voted.

The report will be created according to the [Protection of
Privacy](#protection-of-privacy) guidelines.

The CRB will then contact every individual who is named in the report and ask
each of them if they would like their name to be redacted in the report. The CRB
must allow individuals at least 48 hours to respond to this inquiry. Any
individual who fails to respond in a timely manner is assumed to have asked for
their name to be redacted. All requests for redaction must be honored, with the
following exceptions:

* The CRB can refuse a request for redaction if they determine that such
  redaction would have a serious negative impact on the comprehension of the
  report. This exception can only be used if a majority of the members of the
  CRB vote to approve it. A vote must be called for each instance in which this
  exception is to be used, regardless of the individual involved.
* The names of the members of the CRB who performed any part of the
  investigation cannot be redacted.

No individual may ask for their name to be redacted from the report for the
sole reason of being allowed to participate and/or vote in the related
adjudication meeting. Any person believed to be pursuing this intent may be
barred or ejected from the adjudication meeting at the determination of the CRB.
They may also be subject to further action.

Interview records may be transcribed to make them more accessible. The CRB may
also modify interview records to remove elements that they deem entirely
irrelevant to the investigation.

The CRB may also redact evidence collected during the course of the
investigation as they deem applicable, particularly as it might relate to the
[Protection of Privacy](#protection-of-privacy). Records of interviews with
individuals who request anonymity or whose names have been redacted from the
report will be modified in such a way to remove or sanitize personally
identifying characteristics (e.g. tone, pitch, appearance, vocabulary, etc.).
Modifications for this purpose cannot substantially change the evidence
presented by the record.

The report will, to the greatest reasonable extent, indicate what evidence has
been modified and the nature of the modification.

Prior to distributing the complete report, the CRB will contact each individual
(named or unnamed) who provided evidence included in the report to confirm that
the applicable sections of the report satisfy the individual's requests for
privacy and accurately portrays the evidence which they provided. The section of
the report shared with each party will be limited only to the testimony or
evidence that they provided. If an individual feels that the report does not
meet their requested level of privacy or that any evidence which they provided
has been modified in such a way that it misrepresents what was originally
provided, they should inform the CRB of the deficiency and provide
recommendations to correct it. In the case of a disagreement over modification
of evidence, if the individual and CRB cannot reach an agreement on the manner
in which the evidence is presented, the individual may demand that some or all
of the evidence which they provided be included in the report without
modification (and in so doing waive their right to privacy as it applies to the
evidence in question). The CRB will comply with such a demand so long as doing
so does not compromise the anonymity or integrity of other evidence in the
report. Once these parties approve of the report, the CRB will next repeat this
confirmation process with the Defendant(s), affiliated parties, and other
parties included in the report. The report will not be more broadly distributed
until all involved parties explicitly approve of it.

[]{#eligible-parties}

Within one week of the completion of the investigation, the CRB will distribute
the report (with applicable redactions) to all named and unnamed parties to the
complaint (including the Complainant and the Defendant(s)), as well as the Event
Holders' List. If the CRB is unable to meet this deadline, they will immediately
(and every 7 days thereafter until the report is distributed) contact everyone
to whom the report is to be distributed and provide the following
information:

* The date on which the CRB concluded the investigation.
* The reason for which the CRB has not yet been able to distribute the
  report.
* The date on which the CRB expects to distribute the report.

## Adjudication of Complaints ## {- #adjudication-of-complaints}

After receiving an investigation report from the CRB, the EHs will decide if
the matter is serious enough to warrant an [Emergency EH
Meeting](./Omnibus.html#emergency-eh-meetings){.compatibility}. No sooner than
48 hours and no later than two weeks after the report has been distributed, the
Administrative Meeting Organizers will solicit support for an Emergency EH
Meeting. The purpose of this delay is to allow the EHs time to fully process the
investigation report. If this results in an Emergency EH Meeting, then the
complaint will be adjudicated at that meeting. Otherwise, the complaint will be
adjudicated at the next regular session of the EHC.

At any point before the meeting, the Complainant may contact the ERB to
withdraw their complaint. If this happens, the ERB must let the EHs and all
involved parties know that the complaint has been withdrawn, and the complaint
will no longer be discussed at the meeting. If the meeting was an Emergency EH
Meeting with no other topics, that meeting is canceled.

Regardless of whether it happens at an Emergency EH Meeting or a regular EHC,
adjudication of complaints investigated by the CRB is governed by the policies
and procedures which follow.

Any party named in the investigation report may attend the adjudication portion
of the meeting, regardless of any other eligibility requirements. The
Administrative Meeting Organizers (with the help of the CRB Chairperson or Vice
Chairperson) are responsible for informing these relevant parties of their
eligibility to attend the meeting.

Following the guidelines established above in the [Protection of
Privacy](#protection-of-privacy) section, all participants (regardless of their
eligibility to vote) should only refer to a party by name if they appear in the
report. Redacted names should be referred to by abstract designators. Any person
who, through direct statement or implication, reveals the identity of a redacted
or anonymous party or reveals aspects of other evidence which are redacted
without first securing permission to do so from the applicable party/parties
will be promptly ejected from the adjudication proceedings and may be subject to
further action. The adjudication Moderator has the sole authority to enforce
this policy, but may do so with input from the CRB. An individual who is
anonymous or redacted from the report may voluntarily identify themself, as they
are assumed to have given themself permission. Neither the CRB nor any other
meeting attendee has the authority to compel an anonymous individual to identify
themself.

Neither the Complainant nor the Defendant(s) may participate in votes which are
related to the adjudication of their case. The CRB may determine that other
individuals have an exceptionally close relationship to the case and are thus
also barred from casting votes. Any determination of this nature can be
overruled with a vote by the EHs.

Any other meeting attendee who is named as a party to the case is prohibited
from talking during the meeting unless they are responding to a question which
has been addressed to them. They are not prohibited from casting votes. If the
CRB or the adjudication Moderator feel it would be a valuable contribution to
the discussion, they may allow specific attendees named as parties to the case
to make statements.

Any other meeting attendee may choose to recuse themself from discussion and
voting if they feel that they have a conflict of interest with the case.

Unless otherwise specified, all votes called during the adjudication process
are decided by a 2/3 majority of eligible votes cast, excluding
abstentions.

Prior to the adjudication meeting, the Administrative Meeting Organizers (with
the help of the CRB) will appoint an adjudication Moderator. The Moderator is
responsible for counting votes and managing the flow of discussion.

To start the adjudication meeting, the member of the CRB who is serving as
Chairperson for the investigation revisits the contents of the investigation
report and highlights elements which they feel are particularly important to the
case.

Following the statement by the CRB, the meeting participants will discuss the
case. During this time, they may pose questions to the members of the CRB who
conducted the investigation to clarify aspects of the case. They may also pose
questions to individuals in attendance who are named as parties to the complaint
for the same reason. The Moderator should manage the flow of discussion in such
a way that prioritizes the ability of the CRB and named parties to the complaint
to answer questions directed at them.

Any participant may call for a vote to end the discussion period. A result in
favor of ending discussion moves the adjudication process to the next
stage.

Following the discussion period, the meeting participants will consider what
(if any) action is warranted by the case. Participants may propose actions that
they feel are appropriate for the situation; the proposal must then be seconded
by another participant before it will be taken under consideration. The
adjudication Moderator will keep a record of actions that are under
consideration. For the sake of the language in this section, "taking no further
action" is considered to be an action. A participant who had proposed an action
that is under consideration may choose to remove their proposal from
consideration. This does not prevent another participant from later proposing
the same thing.

As before, the meeting participants may ask the CRB or named parties to clarify
elements of the case. However, the Moderator is responsible for keeping the
questions relevant to the current stage of the adjudication process.

Any participant may call for a vote to end this stage of adjudication if at
least one action is under consideration. A result in favor of ending discussion
moves the adjudication process to the next stage.

After considering possible courses of action, the meeting participants pursue a
particular action in response to the case. First, they use [Approval
Voting](https://en.wikipedia.org/wiki/Approval_voting) to identify the most
popular course of action. Then there will be a confirmation vote on whether to
proceed with that course of action. If the confirmation vote for a course of
action fails, the meeting participants will have discussion to identify why the
vote failed and to pick a new course of action. Once a new course of action has
been chosen, a new confirmation vote will be held.

Finally, once a course of action has been adopted, the CRB determines what (if
anything) needs to be done in order to enforce the action. Within 48 hours of
the conclusion of the adjudication meeting, the CRB will notify all relevant
parties (which may potentially include the Event Holders' List) of the results
of the meeting. If the CRB determines that enforcement is required, then the
notification must also include all of the information that is necessary for
enforcement, which can include: the nature of the action, its effective
duration, conditions under which the action can be relieved, and/or the
player(s) to whom the action applies.

## Community Status Updates ## {- #community-status-updates}

Every quarter the CRB must provide the following information in writing to the
Realms community:

* The total number of complaints submitted in the past quarter.
* The number of complaints withdrawn in this quarter.
* The number of investigations started in the past quarter.
* The number of investigations completed in the past quarter and the amount of
  time required to complete each of them.
* The number of investigations which were closed due to a complaint being
  withdrawn.
* The number of investigations which remain incomplete.

This information will be made publicly available on RealmsNet within 15 days of
the end of each quarter and will remain available for a minimum of ten
years.

For the purposes of this policy, "quarters" are further defined as such:

* First Quarter: January 1 through March 31
* Second Quarter: April 1 through June 30
* Third Quarter: July 1 through September 30
* Fourth Quarter: October 1 through December 31

## Modification of Policies ## {- #modification-of-policies}

The CRB is granted the authority to set and modify policies for itself which
are not already established in the Omnibus or this webpage. Whenever the CRB
utilizes or modifies a policy which is not established in the Omnibus or this
webpage, they must inform the relevant parties.

An [Emergency EH Meeting](./Omnibus.html#emergency-eh-meetings){.compatibility}
has the power to modify CRB policy in any way.

## Challenging CRB Decisions and Policies ## {- #challenging-crb-decisions-and-policies}

If a party to a complaint is subjected to a CRB policy not explicitly
established by the Omnibus or this webpage which they feel is unfair or
inappropriate, they may call for an [Emergency
EH Meeting](./Omnibus.html#emergency-eh-meetings){.compatibility} to attempt to
modify or overrule the policy. If an Emergency EH Meeting is convened for this
reason, the attendees of that meeting have the authority to force the CRB to
make changes to its policies which are not explicitly governed by the
Omnibus.