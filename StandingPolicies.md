---
title: 'Realms Standing Policies'
documentclass: book
geometry: margin=1in
pagestyle: headings
papersize: letter
fontsize: 12pt
toc-depth: 1
...

# COVID Rules At Events # {#covid-rules-at-events}

Enacted on June 12th, 2021 \
Lasts until further notice

## Definitions ## {#definitions}

For the purposes of this Standing Policy, "attendees" shall refer to any
in-person attendee of an event, including but not limited to EHs, staff,
players, and spectators. If the event is held in a location open to the public,
passing members of the public are not attendees unless they substantially engage
with regular attendees. Attendees are to keep clear of non-attendee individuals
to the best of their ability.

Additionally, "regulations" shall refer to any and all federal, state, county,
and local laws, regulations, guidelines, policies, etc. relating to personal
health and safety in the COVID-19 pandemic, which are in effect when a gathering
takes place, and which are applicable to the nature and location(s) of the
event. This definition is expanded when referring to attendees traveling between
jurisdictions prior to, during, or after a gathering to include the relevant
regulations for travel in said jurisdictions and for the full duration of their
travel.

* If melee combat is involved, this includes regulations for "Higher Risk"
  sports and recreational activities.
* If food or drinks are involved, this includes regulations for food service.
* If indoor facilities are involved, this includes regulations for indoor
  gatherings and recreation.

Finally, "event protocols" shall refer to any procedures put in place at an
event to comply with relevant regulations and/or further ensure the health and
safety of attendees.

## For Events and Attendees ## {#for-events-and-attendees}

Every Realms Event must comply with regulations, at a bare minimum. The EH or
designated staff is personally responsible for ensuring compliance.

An EH is within their rights to enact and enforce stricter event protocols than
are required by regulations. Any event must have advertisements that list
or link to any event protocols by at least 2-weeks prior to the event.

Event protocols can be adjusted as needed by EHs up to 72 hours prior to the
event, as regulations change. EHs must state in their
event protocols whether they will enforce a one-warning or zero tolerance policy
for attendees who fail to comply with event protocols or this Standing Policy.

Each attendee is personally responsible for knowing and following all event
protocols for an event, and all travel regulations applicable to their
circumstances. In the case of a minor, their parent, guardian, or a designated
chaperone at the gathering is also personally responsible for their compliance.
Any failures to follow regulations and event protocols must be reported to an
EH or their designated staff member. With the conclusion
of the event, all such reports must be passed along to the CRB by the
gathering's EH.

When any attendee arrives on site, an EH for that event or their designated staff
member must verify the attendee understands and is equipped to follow all event
protocols. If so, the attendee will sign a form stating that they understand,
have followed, and will continue to follow all regulations and event protocols.
No attendee is allowed to participate in or spectate the event until said
form is signed. If any attendee is unable or unwilling to follow any required
regulations or event protocols, they are not allowed to attend the event and
must leave the site. Any such attendee's failure to leave the site promptly must
be reported to the CRB.

The following criteria each prevent someone from attending a Realms event:

* The person has tested positive for COVID-19 in the 10 days before the event.
* The person is required to self-quarantine or self-isolate during the event under local health guidelines
  or as directed by a medical professional.
* The person suspects they have COVID-19 based on any of the following symptoms in the
  14 days prior to the event: fever (> 100.4F) or feeling feverish, new cough, difficulty
  breathing, excessive fatigue, unexpected muscle aches or body aches, new loss
  of smell or taste, sore throat, and diarrhea or vomiting.

# Contact Tracing # {#contact-tracing}

Enacted on June 12th, 2021 \
Lasts until further notice

Every EH is personally responsible for keeping a list of names and email
addresses or phone numbers for any and all attendees at their event, for at
least 14 days after the event's conclusion. Email addresses and phone numbers
are solely for official reasons such as contacting attendees in the case of a
positive COVID test or reporting to the CRB.

Any individual who tests positive for COVID-19 within 5 days of leaving an event must inform
the EH(s) that they have tested positive. The EH(s) must notify the attendees with
their listed contact information of their event within 24 hours as well as keep
any information about the reporter and their circumstances confidential.

# Minimum Attendance # {#minimum-attendance}

Enacted on June 12th, 2021 \
Lasts until further notice

While this policy is in place, the minimum number of attendees for a legal event
is 20 people.

# EH Privileges and Administration # {#eh-privileges-and-administration}

Enacted on June 12th, 2021 \
Lasts until further notice

While this policy is in place, no status relating to being an Event Holder
(including EHC attendance, EHC voting, and membership in the EH mailing list)
expires, and the statuses can only be removed by punitive action or personal
choice.

For the purpose of magic item backing only, any EH that currently has a magic
item on the list is considered to have received credit for holding an event each
year that this standing policy has been in place.

EHCs may be held virtually, rather than in person. Similarly, Players' Meetings
may be held virtually, at each of which Players' Representatives will be elected
to attend the subsequent EHC.

EHs who held a legal event after 2019 must have attended at least 6 or 1/4 of
events (including their own, rounded up) in that year, whichever is fewer, to
gain eligibility to vote (if they were not already eligible to vote).